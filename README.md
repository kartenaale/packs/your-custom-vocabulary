# Your custom vocabulary
Hi, do you want to build your own vocabulary with Anki and want to take
advantage of a feature-rich template without writing any code?

Download this APKG with just one card and then add your own, right within Anki.

The template used in the package is much easier to use than the templates used
for the other packages, where you need to fill in more fields to get all the
functionality.

## Importing the pack to your phone or PC
Open [Deployments | Releases](https://gitlab.phaidra.org/kartenaale/packs/your-custom-vocabulary/-/releases) on the left, then click _Browse all APKGS…_ on
the newest release. There, click the only APKG, download it, and import into Anki.

If you are completely new to Anki and feel a bit lost, check out the user guide
in
[English](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
or
[German](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md).
